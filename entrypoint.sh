#!/bin/bash
cd /var/www/html

# install the files
if [ ! -d "public/application/files/thumbnails" ]; then
	rm -Rf public/application/files
	tar -xzf files.tar.gz -C public/application/
fi

# install custom packages
if [ ! -d "public/packages/engine_room" ]; then
    git submodule update --init --recursive
    cd public/packages/engine_room
    composer install
fi

# ensure we have returned to the working directory
cd /var/www/html

# get the db name
sqlfile=( *.sql )
dbname=${sqlfile%.*}

# wait for db to come up
while ! nc -z db 3306; do sleep 3; done

# check if the db has been installed
if [[ $(mysql -h db -N -s -u root -p'RoC45ev%20' -e "select count(*) from information_schema.tables where table_schema='$dbname' and table_name='Users';") -eq 1 ]]; then
    echo "Database already loaded";
else
    echo "Seeding database from backup ..."
    mysql $dbname -h db -u root -p'RoC45ev%20' < $sqlfile
fi

# run the server
cd public
php -S 0.0.0.0:80 server.php